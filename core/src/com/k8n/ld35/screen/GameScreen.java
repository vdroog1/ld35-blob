package com.k8n.ld35.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.k8n.ld35.LdGame;
import com.k8n.ld35.model.Blob;
import com.k8n.ld35.model.Bullet;
import com.k8n.ld35.model.Enemy;
import com.k8n.ld35.processor.EnemyProcessor;
import com.k8n.ld35.processor.ViewProcessor;
import com.k8n.ld35.state.GameStates;
import com.k8n.ld35.state.MessageType;
import com.k8n.ld35.util.Utils;
import finnstr.libgdx.liquidfun.ParticleBodyContact;
import finnstr.libgdx.liquidfun.ParticleContact;
import finnstr.libgdx.liquidfun.ParticleDebugRenderer;
import finnstr.libgdx.liquidfun.ParticleSystem;

/**
 * Created by kettricken on 16.04.2016.
 */
public class GameScreen extends BaseScreen implements Telegraph {

    private final static float BOX_TO_WORLD = 120.0f;
    public final static float WORLD_TO_BOX = 1f / BOX_TO_WORLD;


    private final static float Y_GAP_MIN = 0.55f;
    private final static float Y_GAP_MAX = 1.8f;
    private final static float X_GAP_MIN = 0.4f;
    private final static float X_GAP_MAX = 4f;
    private final static float X_WIDTH_MIN = 0.5f;
    private final static float X_WIDTH_MAX = 5f;

    private World world;

    private Stage stage;
    private OrthographicCamera stageCamera;
    private ViewProcessor viewProcessor;
    private StateMachine sm;

    private ParticleDebugRenderer mParticleDebugRenderer;
    private Box2DDebugRenderer mDebugRenderer;

    private Vector3 touchPosition= new Vector3();
    private Vector2 linearVelocity = new Vector2();

    private EnemyProcessor enemyProcessor;

    private Blob blob;
    private int button = -1;

    public GameScreen(LdGame game) {
        super(game);
    }

    @Override
    public void show() {
        super.show();

        stageCamera = new OrthographicCamera();
        Viewport viewport = new ExtendViewport(getWorldWidth(), getWorldHeight(), stageCamera);
        stage = new Stage(viewport);

        sm = new DefaultStateMachine(this);
        sm.changeState(GameStates.GAME);

        InputMultiplexer inputMultiplexer = new InputMultiplexer(new ControlProcessor());
        GestureDetector gd = new GestureDetector(new Gestures());
        inputMultiplexer.addProcessor(gd);
        inputMultiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(inputMultiplexer);
        Gdx.input.setCatchBackKey(true);

        touchPosition = new Vector3();

        restart();

        mDebugRenderer = new Box2DDebugRenderer();
        mParticleDebugRenderer = new ParticleDebugRenderer(new Color(0, 1, 0, 1), blob.getParticleSystem().getParticleCount());

        enemyProcessor = new EnemyProcessor(this);
        enemyProcessor.generate();

        viewProcessor = new ViewProcessor(this, stage);
        viewProcessor.initViews();

        world.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {
                Object objectA = contact.getFixtureA().getBody().getUserData();
                Object objectB = contact.getFixtureB().getBody().getUserData();
                if (objectA != null && objectA instanceof Bullet) {
                    Bullet bullet = (Bullet) objectA;
                    if (!(objectB instanceof Enemy) && !(objectB instanceof Bullet) ) {
                        bullet.setAlive(false);
                    }
                }

                if (objectB != null && objectB instanceof Bullet) {
                    Bullet bullet = (Bullet) objectB;
                    if (!(objectA instanceof Enemy) && !(objectA instanceof Bullet)) {
                        bullet.setAlive(false);
                    }
                }
            }
                @Override
            public void endContact(Contact contact) {
            }

            @Override
            public void beginParticleBodyContact(ParticleSystem particleSystem, ParticleBodyContact particleBodyContact) {
                Body body = particleBodyContact.getBody();
                Object object = body.getUserData();
                if (object != null && object instanceof Enemy) {
                    enemyProcessor.eaten(body);
                }

                if (object != null && object instanceof Bullet) {
                    Bullet bullet = (Bullet) object;
                    bullet.setAlive(false);
                    blob.shrink();
                }

            }

            @Override
            public void endParticleBodyContact(Fixture fixture, ParticleSystem particleSystem, int i) {
            }

            @Override
            public void beginParticleContact(ParticleSystem particleSystem, ParticleContact particleContact) {
            }

            @Override
            public void endParticleContact(ParticleSystem particleSystem, int i, int i1) {

            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {
            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {
            }
        });

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        sm.update();
        stage.act(delta);

        if (sm.getCurrentState() == GameStates.GAME) {
            // process
            world.step(Gdx.graphics.getDeltaTime(), 10, 6,
                    blob.getParticleSystem().calculateReasonableParticleIterations(Gdx.graphics.getDeltaTime()));
            enemyProcessor.update();
            blob.update();
        }

        // drawing
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        batch().begin();
        batch().end();

        //Get the combined matrix and scale it down to
        //our Box2D size
        Matrix4 cameraCombined = getCamera().combined.cpy();
        cameraCombined.scale(BOX_TO_WORLD, BOX_TO_WORLD, 1);

        //First render the particles and then the Box2D world
        mParticleDebugRenderer.render(blob.getParticleSystem(), BOX_TO_WORLD, cameraCombined);
        mDebugRenderer.render(world, cameraCombined);

        stage.draw();
    }

    @Override
    public void dispose() {
        super.dispose();
        blob.dispose();
        world.dispose();
        mDebugRenderer.dispose();
        stage.dispose();
    }

    @Override
    protected void layoutViewsLandscape(int width, int height) {

    }

    @Override
    protected void layoutViewsPortrait(int width, int height) {

    }

    private Body createBox2DWorld(float width, float height) {
        if (world == null) {
            world = new World(new Vector2(0, -9.8f), false);
        }

        float halfWidth = width * WORLD_TO_BOX / 2f;
        float halfHeight = height * WORLD_TO_BOX / 2f;

        Vector2[] vertices = new Vector2[] {
                new Vector2(-halfWidth, -halfHeight),
                new Vector2(halfWidth, -halfHeight),
                new Vector2(halfWidth, halfHeight),
                new Vector2(-halfWidth, halfHeight),
                new Vector2(-halfWidth, -halfHeight) };

        ChainShape chainShape = new ChainShape();
        chainShape.createChain(vertices);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = chainShape;
        fixtureDef.isSensor = false;
        fixtureDef.density = 1;
        fixtureDef.friction = 0.5f;
        fixtureDef.restitution = 0;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(new Vector2(halfWidth, halfHeight));

        Body body = world.createBody(bodyDef);
        body.createFixture(fixtureDef);
        body.setUserData("Wall");

        fixtureDef.shape.dispose();

        return body;
    }

    private void createPlatforms() {
        BodyDef bodyDef1 = new BodyDef();
        bodyDef1.type = BodyDef.BodyType.StaticBody;

        FixtureDef fixDef = new FixtureDef();
        fixDef.friction = 0.2f;

        PolygonShape shape = new PolygonShape();

        float lastY = 0;
        while (lastY < getWorldHeight() * WORLD_TO_BOX) {
            float lastX = - getWorldWidth() * WORLD_TO_BOX / 2;
            lastY += Utils.randomFloat(Y_GAP_MIN, Y_GAP_MAX);

            while (lastX < getWorldWidth() * WORLD_TO_BOX) {
                float x = Utils.randomFloat(lastX + X_GAP_MIN, lastX + X_GAP_MAX);
                float width = Utils.randomFloat(X_WIDTH_MIN, X_WIDTH_MAX);
                float halfWidth = width / 2;
                lastX = x + width;

                bodyDef1.position.set(x + halfWidth, lastY);
                Body body = world.createBody(bodyDef1);
                body.setUserData("Platform");

                shape.setAsBox(halfWidth, 0.05f);
                fixDef.shape = shape;

                body.createFixture(fixDef);
            }
        }
        shape.dispose();

    }

    public float getBoxWidth() {
        return getWorldWidth() * WORLD_TO_BOX;
    }

    public float getBoxHeight() {
        return getWorldHeight() * WORLD_TO_BOX;
    }

    public World getWorld() {
        return world;
    }

    public Vector2 getBlobPosition() {
        return blob.getParticleGroup().getCenter();
    }

    public Blob getBlob() {
        return blob;
    }

    public ParticleDebugRenderer getParticleDebugRenderer() {
        return mParticleDebugRenderer;
    }

    public StateMachine getStateMachine() {
        return sm;
    }

    public ViewProcessor getViewProcessor() {
        return viewProcessor;
    }

    public void restart() {
        if (world != null) {
            Array<Body> bodies = new Array<Body>();
            world.getBodies(bodies);
            for (Body body : bodies) {
                world.destroyBody(body);
            }
        }

        createBox2DWorld(getWorldWidth(), getWorldHeight());
        createPlatforms();
        blob = new Blob(this);
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        return sm.handleMessage(msg);
    }

    private class ControlProcessor extends InputAdapter {

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            if (button != Input.Buttons.LEFT) return false;

            GameScreen.this.button = Input.Buttons.LEFT;

            getCamera().unproject(touchPosition.set(screenX, screenY, 0));
            touchPosition.set(touchPosition.x * WORLD_TO_BOX, touchPosition.y * WORLD_TO_BOX, 0);

            linearVelocity.set((touchPosition.x - blob.getParticleGroup().getCenter().x),
                    (touchPosition.y - blob.getParticleGroup().getCenter().y));

            blob.getParticleGroup().applyLinearImpulse(linearVelocity);
            return false;
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            if (GameScreen.this.button != Input.Buttons.LEFT) return false;

            getCamera().unproject(touchPosition.set(screenX, screenY, 0));
            touchPosition.set(touchPosition.x * WORLD_TO_BOX, touchPosition.y * WORLD_TO_BOX, 0);

            linearVelocity.set((touchPosition.x - blob.getParticleGroup().getCenter().x),
                    (touchPosition.y - blob.getParticleGroup().getCenter().y));

            blob.getParticleGroup().applyLinearImpulse(linearVelocity);

            return super.touchDragged(screenX, screenY, pointer);
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            GameScreen.this.button = -1;

            return false;
        }

        @Override
        public boolean keyUp(int keycode) {
            switch (keycode)
            {
                case Input.Keys.ESCAPE:
                case Input.Keys.BACK:
                    Utils.sendMessage(GameScreen.this, GameScreen.this, MessageType.BACK_PRESS);
                    break;
            }
            return true;
        }
    }

    private class Gestures extends GestureDetector.GestureAdapter {
        @Override
        public boolean tap(float x, float y, int count, int button) {
            Utils.sendMessage(GameScreen.this, GameScreen.this, MessageType.TAP);
            return super.tap(x, y, count, button);
        }
    }

}

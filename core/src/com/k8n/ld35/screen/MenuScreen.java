package com.k8n.ld35.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.k8n.ld35.LdGame;
import com.k8n.ld35.data.SkinCache;

/**
 * Created by kettricken on 16.04.2016.
 */
public class MenuScreen extends BaseScreen {

    Stage stage;

    public MenuScreen(LdGame game){
        super(game);
        stage = new Stage();
    }

    @Override
    public void show() {
        super.show();

        Label title = new Label("Blob", SkinCache.getDefaultSkin());

        Table table = new Table();
        table.setFillParent(true);
        table.add(title);
        table.row();

        stage.addActor(table);

        title.addAction(Actions.sequence(Actions.alpha(0), Actions.fadeIn(1)));

        Gdx.input.setInputProcessor(new InputListener());
    }

    public void resize (int width, int height) {
        super.resize(width, height);
        stage.setViewport(getViewport());
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        Gdx.gl.glClearColor( 0, 0, 0, 1 );
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );

        batch().begin();
        batch().end();

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
    }

    @Override
    protected void layoutViewsLandscape(int width, int height) {

    }

    @Override
    protected void layoutViewsPortrait(int width, int height) {

    }

    private class InputListener extends InputAdapter {

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            getGame().setGameScreen();
            return super.touchUp(screenX, screenY, pointer, button);
        }

        @Override
        public boolean keyUp(int keycode) {
            switch (keycode)
            {
                case Input.Keys.ESCAPE:
                case Input.Keys.BACK:
                    Gdx.app.exit();
                    break;
            }
            return true;
        }
    }
}

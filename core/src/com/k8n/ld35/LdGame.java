package com.k8n.ld35;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.k8n.ld35.data.ImageCache;
import com.k8n.ld35.data.MusicCache;
import com.k8n.ld35.data.SkinCache;
import com.k8n.ld35.data.SoundCache;
import com.k8n.ld35.screen.GameScreen;
import com.k8n.ld35.screen.MenuScreen;

public class LdGame extends Game {

	@Override
	public void create () {
		ImageCache.load();
		SkinCache.load();
		SoundCache.load();
		Gdx.input.setCatchBackKey(true);
		setMenuScreen();
	}

	@Override
	public void render () {
		super.render();
	}

	@Override
	public void dispose() {
		Screen screen = getScreen();
		if (screen != null) screen.dispose();
		MusicCache.dispose();
		super.dispose();
	}

	@Override
	public void setScreen(Screen screen) {
		Screen old = getScreen();
		super.setScreen(screen);
		if (old != null) old.dispose();
	}

	public void setMenuScreen() {
		setScreen(new MenuScreen(this));
	}

	public void setGameScreen() {
		setScreen(new GameScreen(this));
	}

	public void exit() {
		Gdx.app.exit();
	}
}

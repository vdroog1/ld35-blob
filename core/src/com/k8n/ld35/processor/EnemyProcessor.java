package com.k8n.ld35.processor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.k8n.ld35.model.Bullet;
import com.k8n.ld35.model.Enemy;
import com.k8n.ld35.screen.GameScreen;
import com.k8n.ld35.util.Utils;

/**
 * Created by kettricken on 16.04.2016.
 */
public class EnemyProcessor {

    Array<Body> enemies = new Array<Body>();

    GameScreen gameScreen;

    private float halfSize = 0.1f;
    private float spawnTime = 0;

    private Vector2 linearVelocity = new Vector2();
    private Vector2 shootingDirection = new Vector2();
    private final Array<Body> toRemove = new Array<Body>();

    private final Array<Bullet> activeBullets = new Array<Bullet>();
    private final Pool<Bullet> bulletPool = new Pool<Bullet>() {
        @Override
        protected Bullet newObject() {
            return new Bullet(gameScreen.getWorld());
        }
    };

    public EnemyProcessor(GameScreen gameScreen) {
        this.gameScreen = gameScreen;
    }

    public void generate() {
        BodyDef bodyDef1 = new BodyDef();
        bodyDef1.type = BodyDef.BodyType.DynamicBody;

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(halfSize, halfSize);

        FixtureDef fixDef = new FixtureDef();
        fixDef.friction = 0.2f;
        fixDef.shape = shape;

        Vector2 location = new Vector2();
        int amount = Utils.randInt(3, 8);
        for (int i = 0; i < amount; i++) {
            location.set(Utils.randomFloat(halfSize * 2, gameScreen.getBoxWidth() - halfSize * 2),
                    Utils.randomFloat(halfSize * 2, gameScreen.getBoxHeight() - halfSize * 2));
            bodyDef1.position.set(location.x, location.y);
            Body body = gameScreen.getWorld().createBody(bodyDef1);
            body.createFixture(fixDef);
            Enemy enemy = new Enemy();
            body.setUserData(enemy);
        }
        shape.dispose();
    }

    public void update() {
        spawnTime += Gdx.graphics.getDeltaTime();
        for (Body body: toRemove) {
            gameScreen.getWorld().destroyBody(body);
            gameScreen.getBlob().grow();
        }
        toRemove.clear();

        gameScreen.getWorld().getBodies(enemies);
        int enemiesCount = 0;
        for (Body body: enemies) {
            if (body.getUserData() != null && body.getUserData() instanceof Enemy) {
                enemiesCount++;
                Enemy enemy = (Enemy) body.getUserData();
                linearVelocity.set((gameScreen.getBlobPosition().x - body.getPosition().x),
                        (gameScreen.getBlobPosition().y - body.getPosition().y));
                shootingDirection.set(linearVelocity.x, linearVelocity.y);

                float distance = Utils.getDistance(gameScreen.getBlobPosition().x, gameScreen.getBlobPosition().y,
                        body.getPosition().x, body.getPosition().y);

                if (distance < 3.5f) {
                    linearVelocity.scl(-1);
                }
                if (distance <= 4f) {
                    enemy.setShooting(true);
                } else {
                    enemy.setShooting(false);
                }

                enemy.update();
                if (enemy.isShooting() && enemy.getBulletSpawnTime() >= enemy.getBulletsDelta()) {
                    enemy.resetSpawnTime();
                    Bullet item = bulletPool.obtain();
                    item.init(body.getPosition(), shootingDirection);
                    activeBullets.add(item);
                }
                body.setLinearVelocity(linearVelocity);
            }
        }

        int len = activeBullets.size;
        for (int i = len; --i >= 0;) {
            Bullet item = activeBullets.get(i);
            if (!item.isAlive()) {
                item.destroy();
                activeBullets.removeIndex(i);
                bulletPool.free(item);
            }
        }

        if ((enemiesCount < 3 && spawnTime > 10 ) || enemiesCount == 0) {
            spawnTime = 0;
            generate();
        }
    }

    public void draw() {
        gameScreen.getWorld().getBodies(enemies);
        for (Body body: enemies) {
            if (body.getUserData() != null && body.getUserData() instanceof Enemy) {
            }
        }
    }

    public void eaten(Body body) {
        if (toRemove.contains(body, true)) {
            return;
        }
        toRemove.add(body);
    }

    public void restart() {
        spawnTime = 0;
        toRemove.clear();

        gameScreen.getWorld().getBodies(enemies);
        for (Body body: enemies) {
            if (body.getUserData() != null && body.getUserData() instanceof Enemy) {
                gameScreen.getWorld().destroyBody(body);
            }
        }

        int len = activeBullets.size;
        for (int i = len; --i >= 0;) {
            Bullet item = activeBullets.get(i);
            if (!item.isAlive()) {
                item.destroy();
                activeBullets.removeIndex(i);
                bulletPool.free(item);
            }
        }

        generate();
    }
}

package com.k8n.ld35.processor;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.k8n.ld35.data.SkinCache;
import com.k8n.ld35.screen.GameScreen;
import com.k8n.ld35.state.MessageType;
import com.k8n.ld35.util.Utils;

/**
 * Created by kettricken on 17.04.2016.
 */
public class ViewProcessor {

    private final GameScreen gameScreen;
    private final Stage stage;

    private Table pauseTable = new Table();
    private Table gameOverTable = new Table();
    private Table infoTable = new Table();

    private Label gameOver;
    private Label restart, pauseRestart;
    private Label wave;

    private boolean restartEnabled = false;

    public ViewProcessor(GameScreen gameScreen, Stage stage) {
        this.stage = stage;
        this.gameScreen = gameScreen;
    }

    public void initViews() {
        gameOverTable.setFillParent(true);
        pauseTable.setFillParent(true);
        infoTable.setFillParent(true);
        infoTable.center();

        gameOver = new Label("Game Over", SkinCache.getDefaultSkin());
        Label pause = new Label("Pause", SkinCache.getDefaultSkin());
        Label continueBtn = new Label("Continue", SkinCache.getDefaultSkin());
        Label exitBtn = new Label("Exit", SkinCache.getDefaultSkin());
        restart = new Label("Restart", SkinCache.getDefaultSkin());
        pauseRestart = new Label("Restart", SkinCache.getDefaultSkin());
        wave = new Label(String.format("Size %d/100", (int) (gameScreen.getBlob().getRadius() * 100)), SkinCache.getDefaultSkin());
        //wave.getStyle().font.getData().scale(0.7f);
        //wave.getStyle().fontColor = new Color(1f, 0.6f, 0.1f, 1);
        gameOverTable.add(gameOver);
        gameOverTable.row();
        gameOverTable.add(restart);
        pauseTable.add(pause).colspan(2).center().spaceBottom(50);
        pauseTable.row();
        pauseTable.add(continueBtn).spaceRight(50);
        pauseTable.add(exitBtn);
        pauseTable.row();
        pauseTable.add(pauseRestart).colspan(2).center();
        infoTable.add(wave);
        infoTable.top();

        continueBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                Utils.sendMessage(gameScreen, gameScreen, MessageType.BACK_PRESS);
            }
        });

        exitBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                gameScreen.getGame().exit();
            }
        });

        pauseRestart.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                Utils.sendMessage(gameScreen, gameScreen, MessageType.RESTART);
            }
        });

        gameOverTable.addAction(Actions.alpha(0));
        stage.addActor(gameOverTable);

        stage.addActor(infoTable);
    }

    public void showPauseTable() {
        stage.addActor(pauseTable);
        pauseTable.addAction(Actions.sequence(Actions.alpha(0), Actions.fadeIn(0.15f), new Action() {
            @Override
            public boolean act(float delta) {
                pauseTable.setTouchable(Touchable.enabled);
                return true;
            }
        }));
    }

    public void hidePauseTable() {
        Action action = new Action() {
            @Override
            public boolean act(float delta) {
                pauseTable.getParent().removeActor(pauseTable);
                return true;
            }
        };
        pauseTable.setTouchable(Touchable.disabled);
        pauseTable.addAction(Actions.sequence(Actions.fadeOut(0.15f), action));
    }

    public void showGameOverTable(boolean result) {
        if (result) {
            gameOver.setText("You are fat enough to survive!");
        } else {
            gameOver.setText("You are extinct!");
        }
        restart.addAction(Actions.alpha(0));
        gameOver.addAction(Actions.alpha(0));
        gameOverTable.addAction(Actions.alpha(1));
        gameOver.addAction(Actions.sequence(Actions.fadeIn(0.20f), new Action() {
            @Override
            public boolean act(float delta) {
                restart.addAction(Actions.alpha(1));
                restartEnabled = true;
                return true;
            }
        }));
    }

    public void hideGameOverTable() {
        gameOverTable.addAction(Actions.alpha(0));
        restartEnabled = false;
    }

    public boolean isRestartEnabled() {
        return restartEnabled;
    }

    public void updateSize(float radius) {
        wave.setText(String.format("Size %d/100", (int) (radius * 100)));
    }

    public void restart() {
        wave.setText(String.format("Size %d/100", (int) (gameScreen.getBlob().getRadius() * 100)));
    }
}

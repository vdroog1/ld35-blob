package com.k8n.ld35.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Created by kettricken on 16.04.2016.
 */
public class Bullet {

    private static final CircleShape circleShape = new CircleShape();

    private boolean alive;
    private Body body;
    private final FixtureDef fixtureDef;
    private final BodyDef bodyDef;
    private final World world;

    public Bullet(World world) {
        this.world = world;

        circleShape.setRadius(0.02f);

        fixtureDef = new FixtureDef();
        fixtureDef.shape = circleShape;
        fixtureDef.isSensor = false;
        fixtureDef.density = 0.01f;

        bodyDef = new BodyDef();
        bodyDef.bullet = true;
        bodyDef.type = BodyDef.BodyType.DynamicBody;

        //circleShape.dispose();

        this.alive = false;
    }

    public void init(Vector2 position, Vector2 direction) {
        this.body = world.createBody(bodyDef);
        body.createFixture(fixtureDef);
        body.setUserData(this);

        body.setTransform(position, 0);
        body.setLinearVelocity(direction.scl(10));
        alive = true;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void destroy() {
        this.world.destroyBody(body);
        body = null;
    }

}

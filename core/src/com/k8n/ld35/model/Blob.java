package com.k8n.ld35.model;

import com.badlogic.gdx.physics.box2d.CircleShape;
import com.k8n.ld35.screen.GameScreen;
import com.k8n.ld35.state.GameStates;
import com.k8n.ld35.util.Utils;
import finnstr.libgdx.liquidfun.*;

/**
 * Created by kettricken on 16.04.2016.
 */
public class Blob {

    private final static float BLOB_SIZE = 0.4f;
    private final static float BLOB_SIZE_DELTA = 0.05f;
    private final static float BLOB_NEGATIVE_DELTA = 0.025f;
    private static final float DAMPING_DELTA = 0.1f;

    private GameScreen gameScreen;
    private ParticleGroupDef mParticleGroupDef1;
    private ParticleGroup particleGroup;
    private ParticleSystem particleSystem;

    private float radius = 0;
    private float newRadius = 0;
    private float damping = 0.1f;

    public Blob(GameScreen gameScreen) {
        this.gameScreen = gameScreen;

        radius = BLOB_SIZE / 2f;
        newRadius = radius;

        ParticleSystemDef systemDef = new ParticleSystemDef();
        systemDef.radius = 2f * GameScreen.WORLD_TO_BOX; // of single particle
        systemDef.dampingStrength = damping;

        particleSystem = new ParticleSystem(gameScreen.getWorld(), systemDef);
        particleSystem.setParticleDensity(1.3f);

        mParticleGroupDef1 = new ParticleGroupDef();
        mParticleGroupDef1.color.set(1f, 0, 0, 1);
        mParticleGroupDef1.flags.add(ParticleDef.ParticleType.b2_elasticParticle);
        mParticleGroupDef1.flags.add(ParticleDef.ParticleType.b2_fixtureContactListenerParticle);
        mParticleGroupDef1.position.set(Utils.randomFloat(radius + 0.1f, gameScreen.getBoxWidth() - radius - 0.1f),
                Utils.randomFloat(radius + 0.1f, gameScreen.getBoxHeight() - radius - 0.1f));

        //The shape defines where the particles are created exactly
        //and how much are created

        CircleShape parShape = new CircleShape();
        parShape.setRadius(radius);
        mParticleGroupDef1.shape = parShape;
        particleGroup = particleSystem.createParticleGroup(mParticleGroupDef1);

        particleGroup.setUsetData(this);
    }

    public void grow() {
        newRadius = radius + BLOB_SIZE_DELTA;
        damping += DAMPING_DELTA;

    }

    public ParticleGroup getParticleGroup() {
        return particleGroup;
    }

    public ParticleSystem getParticleSystem() {
        return particleSystem;
    }

    public void dispose() {
        mParticleGroupDef1.shape.dispose();
    }

    public void shrink() {
        newRadius  = radius - BLOB_NEGATIVE_DELTA;
        damping -= DAMPING_DELTA;
    }

    public void update() {
        if (newRadius == radius) {
            return;
        }

        if (radius >= 1f) {
            gameScreen.getStateMachine().changeState(GameStates.GAME_WON);
            return;
        } else if (radius <= 0.1f) {
            gameScreen.getStateMachine().changeState(GameStates.GAME_OVER);
            return;
        }

        particleSystem.setParticleDamping(damping);
        radius = newRadius;
        CircleShape parShape = new CircleShape();
        parShape.setRadius(radius);
        mParticleGroupDef1.shape = parShape;
        float x = particleGroup.getCenter().x;
        float y = particleGroup.getCenter().y;
        if (y < radius) {
            y = radius + 0.1f;
        } else if (y > gameScreen.getBoxHeight() - radius) {
            y = gameScreen.getBoxHeight() - radius - 0.1f;
        }

        if (x < radius) {
            x = radius + 0.1f;
        } else if (x > gameScreen.getBoxWidth() - radius) {
            x = gameScreen.getBoxWidth() - radius - 0.1f;
        }
        mParticleGroupDef1.position.set(x, y);
        particleGroup.destroyParticlesInGroup();
        particleGroup = particleSystem.createParticleGroup(mParticleGroupDef1);
        if(particleSystem.getParticleCount() > gameScreen.getParticleDebugRenderer().getMaxParticleNumber()) {
            gameScreen.getParticleDebugRenderer().setMaxParticleNumber(particleSystem.getParticleCount() + 1000);
        }

        gameScreen.getViewProcessor().updateSize(radius);
    }

    public float getRadius() {
        return radius;
    }
}

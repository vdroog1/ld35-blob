package com.k8n.ld35.model;

import com.badlogic.gdx.Gdx;
import com.k8n.ld35.util.Utils;

/**
 * Created by kettricken on 16.04.2016.
 */
public class Enemy {
    private boolean shooting;
    private float lastSpawnTime = 0;
    private float bulletsDelta;

    public Enemy() {
        bulletsDelta = Utils.randomFloat(0.2f, 0.5f);
    }

    public void setShooting(boolean shooting) {
        this.shooting = shooting;
    }

    public void update() {
        lastSpawnTime += Gdx.graphics.getDeltaTime();
    }

    public boolean isShooting() {
        return shooting;
    }

    public float getBulletSpawnTime() {
        return lastSpawnTime;
    }

    public void resetSpawnTime() {
        lastSpawnTime = 0;
    }

    public float getBulletsDelta() {
        return bulletsDelta;
    }
}

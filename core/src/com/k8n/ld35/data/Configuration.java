package com.k8n.ld35.data;

public class Configuration {
    public static final int GAME_WIDTH = 720;
    public static final int GAME_HEIGHT = 1280;
    public static final float SCALE_FACTOR = 1;
}
